## electronic-io (noetic) - 1.0.3-1

The packages in the `electronic-io` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic electronic-io` on `Mon, 03 Jul 2023 13:14:05 -0000`

These packages were released:
- `electronic_io`
- `electronic_io_msgs`

Version of package(s) in repository `electronic-io`:

- upstream repository: https://github.com/ctu-vras/electronic-io.git
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/electronic-io.git
- rosdistro version: `1.0.1-1`
- old version: `1.0.2-1`
- new version: `1.0.3-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## electronic-io (noetic) - 1.0.2-1

The packages in the `electronic-io` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic electronic-io` on `Fri, 30 Jun 2023 16:38:58 -0000`

These packages were released:
- `electronic_io`
- `electronic_io_msgs`

Version of package(s) in repository `electronic-io`:

- upstream repository: https://github.com/ctu-vras/electronic-io.git
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/electronic-io.git
- rosdistro version: `1.0.1-1`
- old version: `1.0.1-1`
- new version: `1.0.2-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## electronic-io (noetic) - 1.0.1-1

The packages in the `electronic-io` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic -t noetic --new-track electronic-io` on `Sun, 18 Jun 2023 23:04:50 -0000`

These packages were released:
- `electronic_io`
- `electronic_io_msgs`

Version of package(s) in repository `electronic-io`:

- upstream repository: https://github.com/ctu-vras/electronic-io.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `1.0.1-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## electronic-io (melodic) - 1.0.1-1

The packages in the `electronic-io` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic -t melodic --new-track electronic-io` on `Sun, 18 Jun 2023 23:01:15 -0000`

These packages were released:
- `electronic_io`
- `electronic_io_msgs`

Version of package(s) in repository `electronic-io`:

- upstream repository: https://github.com/ctu-vras/electronic-io.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `1.0.1-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


# electronic-io Release repo
